Project Name: GameCity E-Commerce API

Features:
User registration
User authentication
Set user as admin (Admin only)


Registration:
POST - http://localhost:4000/register

Body: (JSON)
{
	"firstName" : String,
	"lastName" : String,
	"email" : String,
	"password" : String,
	"mobileNo" : String
}


Login:
POST - http://localhost:4000/login

Body: (JSON)
{
	"email" : String,
	"password" : String
}

Set as Admin:
PUT - http://localhost:4000/setAsAdmin/Id

Body: No request Body
Admin token required.

Admin Credentials:

email: "adminAPI@gmail.com",
password: "adminAPI123"
