const express = require('express');

const router = express.Router();

const auth = require ('../auth');

const {verify,verifyAdmin} = auth

const userControllers = require('../controllers/userControllers')

const {
	register,
	loginUser,
	setAsAdmin,
	getSingleUserController,
	checkoutController,
	orderController
} = userControllers

//add User
router.post('/register',register)
//login User
router.post('/login',loginUser)
//set As Admin
router.put('/setAsAdmin/:id',verify,verifyAdmin,setAsAdmin)
router.get('/userDetails',verify,getSingleUserController)
router.post('/checkout',checkoutController)
router.post('/orders/:id',orderController)


module.exports = router