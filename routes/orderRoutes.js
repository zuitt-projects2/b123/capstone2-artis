const express = require('express');

const router = express.Router();

const auth = require ('../auth');

const {verify,verifyAdmin} = auth

const orderControllers = require('../controllers/orderControllers')

const {
	createOrder,
	getUsersOrders,
	getAllOrders,

} = orderControllers

//Create Order
router.post('/createOrder',verify ,createOrder)
//get authenticated user's orders
router.get('/getUsersOrders',verify, getUsersOrders)
//get all orders (admin only)
router.get('/getAllOrders',verify, verifyAdmin, getAllOrders)

module.exports = router
