const express = require('express');

const router = express.Router();

const auth = require ('../auth');

const {verify,verifyAdmin} = auth

const productControllers = require('../controllers/productControllers')

const {
	addProducts,
	activeProducts,
	singleProducts,
	updateProducts,
	archiveProducts,
	activateProducts,
	searchNameProducts,
	allProducts
}= productControllers
//add Products (admin only)
router.post('/addProducts',verify, verifyAdmin, addProducts)
//get active products
router.get('/activeProducts',activeProducts)
//get single products
router.get('/singleProducts/:id',singleProducts)
//update product information (admin only)
router.put('/updateProducts/:id',verify, verifyAdmin, updateProducts)
//archiveProducts (admin only)
router.put('/archiveProducts/:id',verify, verifyAdmin, archiveProducts)
//activateProduct (admin only)
router.put('/activateProducts/:id',verify, verifyAdmin, activateProducts)
//search name Product
router.get('/search/:name',searchNameProducts)
router.get('/',verify,verifyAdmin,allProducts)


module.exports = router