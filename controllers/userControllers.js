const User = require('../models/User')

const bcrypt = require('bcrypt')

const auth = require('../auth')

const {createAccessToken} = auth

module.exports.register = (request,response) => {

	User.findOne({email:request.body.email}).then(result => {
		if(result !== null) {
			return response.send({
				message:"Email is already registered"})
		} else {

			if(request.body.password.length <8) return response.send({message:"Password is too short"})

			const hashedPW = bcrypt.hashSync(request.body.password,10);
			
			let newUser = new User ({

				firstName:request.body.firstName,
				lastName:request.body.lastName,
				mobileNo:request.body.mobileNo,
				email:request.body.email,
				password:hashedPW,
				confirmPassword:request.body.confirmPassword

			})

			newUser.save()
			.then(user => response.send(user))
			.catch(err => response.send(false))
		}
	}).catch(err => response.send(err))
}

module.exports.loginUser = (request,response) => {

	User.findOne({email : request.body.email})
	.then(emailFound => {
		
		if(emailFound === null){
			return response.send({message:"No User Found"})

		} else {
			const isPasswordCorrect = bcrypt.compareSync(request.body.password,emailFound.password)

			if(isPasswordCorrect){
				return response.send({accessToken:createAccessToken(emailFound)})
			} else {
				return response.send({message:"Incorrect Password"})
			}
		}
	})
	.catch(err => response.send(err))
}

module.exports.setAsAdmin = (request, response) => {

	User.findByIdAndUpdate(request.params.id,{isAdmin:true},{new:true})
	.then(updatedUser => response.send(updatedUser)).catch(err => response.send(err))


}

module.exports.getSingleUserController = (request,response) => {

	//logged in user's details after decoding with auth module's verify()
	console.log(request.user)
	User.findById(request.user.id)
	.then(result => response.send(result))
	.catch(err => response.send(err))
}
module.exports.checkoutController = async (request,response) => {


	if (request.user.isAdmin) return response.send({
		auth : "Failed",
		message:"Action Forbidded"})

	let isProductUpdated = await Product.findOne(request.body.products.name).then(product => {

		Order.products.push({productsId: request.product.id})

		return product.save()
		.then(user => true)
		.catch(err => err.message)
	})
	console.log(isProductUpdated)
	if(isProductUpdated !== true) return response.send({message:isProductUpdated});
}

module.exports.orderController = async (request,response) => {


	if (request.user.isAdmin) return response.send({
		auth : "Failed",
		message:"Action Forbidded"})

let isUserUpdated = await User.findById(request.user.id).then(user => {

	user.orders.push({productId:request.body.productId})

	return user.save()
	.then(user => true)
	.catch(err => err.message)
	})

	console.log(isUserUpdated)
	if(isUserUpdated !== true) return response.send({message:isUserUpdated})

	let isOrderUpdated = await Order.findById(request.body.productId).then(order => {

		order.products.push({userId: request.user.id})

		return order.save()
		.then(user => true)
		.catch(err => err.message)
	})
	console.log(isOrderUpdated)
	if(isOrderUpdated !== true) return response.send({message:isOrderUpdated});

	if(isUserUpdated && isCourseUpdated) return response.send ({message:"Added to Cart"})
}