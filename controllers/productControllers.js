const Product = require('../models/Product')

const auth = require('../auth')

const {createAccessToken} = auth

module.exports.addProducts = (request,response) => {
		let newProducts = new Product ({

		name:request.body.name,
		description:request.body.description,
		price:request.body.price,
		fileInput:request.body.fileInput

	})
	newProducts.save()
	.then(addedProducts => response.send(true))
	.catch(err => response.send(false))

}


module.exports.activeProducts = (request,response) => {
	Product.find({isActive:true})
	.then(foundProduct => response.send(foundProduct))
	.catch(err => response.send(err))
}

module.exports.singleProducts = (request,response) => {

	Product.findById(request.params.id)
	.then(foundProduct => response.send(foundProduct))
	.catch(err => response.send(err))

}

module.exports.updateProducts = (request,response) => {

	let updates = {

	 	name:request.body.name,
	 	description:request.body.description,
	 	price:request.body.price,
	 	fileInput:request.body.fileInput
	}

	 Product.findByIdAndUpdate(request.params.id,updates,{new:true})
	 .then(updatedProduct => response.send(true)).catch(err => response.send(false))
}

module.exports.archiveProducts = (request,response) => {
 
	Product.findByIdAndUpdate(request.params.id,{isActive:false},{new:true})
	 .then(archiveProduct => response.send(true))
	 .catch(err => response.send(false))
}
module.exports.activateProducts = (request,response) => {
 
	Product.findByIdAndUpdate(request.params.id,{isActive:true},{new:true})
	 .then(activateProduct => response.send(true))
	 .catch(err => response.send(false))
}
module.exports.searchNameProducts = (request,response) => {

	Product.find({name:request.params.name}).then(foundProducts => response.send(foundProducts))
	.catch(err => response.send(err))
}
module.exports.allProducts = (req,res) => {

	Product.find()
	.then(foundCourses =>res.send(foundCourses))
	.catch(error => res.send(error))

}
