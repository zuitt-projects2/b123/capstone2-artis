const User = require('../models/User');
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require('bcrypt');

const auth = require('../auth');
const {createAccessToken} = auth;

module.exports.createOrder = (request,response) => {


	if(request.user.isAdmin) 
		return response.send({
			auth:"Failed",
			message:"Action Forbidden"
		})

		

	let newOrder = new Order ({

		userId:request.user.id,
		products:request.body.products
		
	})


	newOrder.save()
	.then(order => response.send(true))
	.catch(err => response.send(false))



}
module.exports.getUsersOrders = (request,response) => {

	if(request.user.isAdmin) 
		return response.send({
			auth:"Failed",
			message:"Action Forbidden"
		})

	Order.find({userId:request.user.id})
	.then(usersOrders => response.send(usersOrders))
	.catch(err => response.send(err))
}

module.exports.getAllOrders = (request,response) => {

	Order.find()
	.then(order => response.send(order))
	.catch(err => response.send(err))
}
