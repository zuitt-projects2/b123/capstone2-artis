const jwt = require('jsonwebtoken')
const secret = "GameCity"

module.exports.createAccessToken = (user) => {

	const data = {

		id:user._id,
		email:user.email,
		isAdmin:user.isAdmin
	}

	return jwt.sign(data,secret,{})
}

module.exports.verify = (request, response, next) => {

	let token = request.headers.authorization
	
	if(typeof token === "undefined") {

		return response.send({})
	} else {
		
		token = token.slice(7,token.length);
	
		jwt.verify(token,secret,function(err,decodedToken) {

			if(err){

				return response.send({auth:"Failed", message:err.message})
			} else {

				request.user = decodedToken;
				next()
			}
		})
	}

}

module.exports.verifyAdmin = (request,response,next) => {

	if(request.user.isAdmin) {

		next()
	} else {

		return response.send({
			auth:"Failed",
			message:"Action Forbidden Not Admin"
		})
	}
}