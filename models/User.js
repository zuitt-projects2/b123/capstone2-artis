const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true,"First Name is required"]
	},
	lastName: {
		type: String,
		required: [true,"Last Name is required"]
	},
	mobileNo: {
		type: String,
		required: [true,"MobileNo is required"]
	},
	email: {
		type: String,
		required: [true,"Email is required"]
	},
	password: {
		type: String,
		required: [true,"Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default : false
	},
	orders: [
		{
			productName:{
				type: String,
				required : [true, "Product Name is required"]
			},
			OrderedON : {
				type : Date,
				default : new Date()
			},
			isActive: {
				type : Boolean,
				default : true
				}
			}
		]

})
module.exports = mongoose.model("User", userSchema)