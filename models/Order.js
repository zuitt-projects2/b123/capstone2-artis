const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({

	datePurchased: {
		type: Date,
		default: new Date()
	},
	userId: {
		type: String,
		required: [true,"User ID is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	products: [
		
		{
			name:{
				type: String,
				required: [true,"Name is required"]
			},
			quantity:{
				type: Number,
				required: [true,"Quantity is required"]
			}

		}


	]

})
module.exports = mongoose.model("Order", orderSchema)