const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
 
const app = express();
const port = process.env.PORT || 4000;

mongoose.connect("mongodb+srv://ianartis29:ianartis@cluster0.mngou.mongodb.net/GameCityAPI?retryWrites=true&w=majority",
	{

		useNewUrlParser: true,
		useUnifiedTopology: true

	}
);

let db = mongoose.connection;
db.on("error",console.error.bind(console,"Connection Error"));
db.once("open",()=>console.log("Connected to MongoDB"));

app.use(cors());

app.use(express.json());

const orderRoutes = require('./routes/orderRoutes')
app.use('/orders',orderRoutes)

const productRoutes = require('./routes/productRoutes')
app.use('/products',productRoutes)

const userRoutes = require('./routes/userRoutes')
app.use('/users',userRoutes)

app.listen(port, () => console.log(`Server running at port ${port}`))